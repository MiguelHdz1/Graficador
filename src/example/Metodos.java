
package example;

import java.awt.*;
import javax.swing.*;

/**
 *
 * @author mickey
 */
public class Metodos {

    public void Plano(JPanel panel) {

        Graphics2D s = (Graphics2D) panel.getGraphics();
        int aumento = 0;

        for (int i = 0; i <= panel.getHeight(); i++) {

            s.setColor(Color.lightGray);
            s.drawLine(0, aumento, panel.getWidth(), aumento);
            s.drawLine(aumento, 0, aumento, panel.getHeight());

            aumento += 30;
        }

        Stroke ancholapiz = new BasicStroke(2);
        s.setStroke(ancholapiz);
        s.setColor(Color.RED);
        s.drawLine(panel.getWidth() / 2, 0, panel.getWidth() / 2, panel.getHeight());
        s.drawLine(0, panel.getHeight() / 2, panel.getWidth(), panel.getHeight() / 2);

    }

    public void PuntoX(JPanel panel) {
        Stroke ancholapiz = new BasicStroke(2);

        Graphics2D s = (Graphics2D) panel.getGraphics();
        int puntox = panel.getWidth() / 2;
        int puntoy = panel.getHeight() / 2;
        s.setFont(new Font("TimesRoman", Font.BOLD, 10));

        s.setStroke(ancholapiz);
//------------------------------------------------------------------------------        
//------------------------------------------------------------------------------
        int dismX = 0;

        for (int i = 0; i <= puntox; i++) {
            s.drawLine(puntox - dismX, puntoy - 6, puntox - dismX, puntoy + 6);
            s.drawString("" + ((dismX / 30) * -1), puntox - dismX - 5, puntoy + 15);
            dismX += 30;
        }
//------------------------------------------------------------------------------        
        int dismX2 = 0;
        for (int i = panel.getHeight() / 2; i <= panel.getHeight(); i++) {
            s.drawLine(puntox + dismX2, puntoy - 6, puntox + dismX2, puntoy + 6);
            dismX2 += 30;
            s.drawString("" + (dismX2 / 30), puntox + dismX2 - 5, puntoy + 15);
        }
//------------------------------------------------------------------------------
        int dismY = 0;
        for (int i = 0; i <= puntoy; i++) {
            s.drawLine(puntox - 6, puntoy - dismY, puntox + 6, puntoy - dismY);
            dismY += 30;
            s.drawString("" + (dismY / 30), puntox - 15, puntoy - dismY);
        }
//------------------------------------------------------------------------------
        int dismY2 = 0;
        for (int i = 0; i <= puntoy; i++) {
            s.drawLine(puntox - 6, puntoy + dismY2, puntox + 6, puntoy + dismY2);
            dismY2 += 30;
            s.drawString("" + ((dismY2 / 30) * -1), puntox - 20, puntoy + dismY2);
        }

    }

    public void GraficarSen(JPanel panel, double Inicio, double Final) {
        Graphics2D s = (Graphics2D) panel.getGraphics();
        s.clearRect(0, 0, panel.getWidth(), panel.getHeight());
        s.setColor(Color.white);
        s.fillRect(0, 0, panel.getWidth(), panel.getHeight());
        Plano(panel);
        PuntoX(panel);
        int Xcero = panel.getWidth() / 2;
        int Ycero = panel.getHeight() / 2;

        Stroke ancholapiz = new BasicStroke(2.5f);
        s.setStroke(ancholapiz);
        s.setColor(Color.orange);

        for (double i = Inicio; i <= Final + .1; i += .001) {

            double y = Math.sin(i) * 30;
            double x = i*30;
            s.drawLine((int) (Xcero + x ), (int) (Ycero - (y)), (int) (Xcero + x), (int) (Ycero - (y)));

        }

    }

    public void GraficarCos(JPanel panel, double Inicio, double Final) {
        Graphics2D s = (Graphics2D) panel.getGraphics();
        s.clearRect(0, 0, panel.getWidth(), panel.getHeight());
        s.setColor(Color.white);
        s.fillRect(0, 0, panel.getWidth(), panel.getHeight());
        Plano(panel);
        PuntoX(panel);
        int Xcero = panel.getWidth() / 2;
        int Ycero = panel.getHeight() / 2;

        Stroke ancholapiz = new BasicStroke(2.5f);
        s.setStroke(ancholapiz);
        s.setColor(Color.orange);

        for (double i = Inicio; i <= Final + .1; i += .001) {

            double y = Math.cos(i) * 30;
            double x = i*30;
            s.drawLine((int) (Xcero + x ), (int) (Ycero - (y)), (int) (Xcero + x ), (int) (Ycero - (y)));

        }

    }

    public void GraficarTan(JPanel panel, double Inicio, double Final) {
        Graphics2D s = (Graphics2D) panel.getGraphics();
        s.clearRect(0, 0, panel.getWidth(), panel.getHeight());
        s.setColor(Color.white);
        s.fillRect(0, 0, panel.getWidth(), panel.getHeight());
        Plano(panel);
        PuntoX(panel);
        int Xcero = panel.getWidth() / 2;
        int Ycero = panel.getHeight() / 2;

     
        Stroke ancholapiz = new BasicStroke(2.5f);
        s.setStroke(ancholapiz);
        s.setColor(Color.orange);

        for (double i = Inicio; i <= Final + .1; i += .001) {

            double y = Math.tan(i) * 30;
            double x = i*30;
            s.drawLine((int) (Xcero + x), (int) (Ycero - (y)), (int) (Xcero + x), (int) (Ycero - (y)));

        }

    }

}
